from flask import Flask
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address

app = Flask(__name__)
limiter = Limiter(
    get_remote_address,
    app=app,
    default_limits=["200 per day", "50 per hour"],
    storage_uri="memcached://localhost:22122",
)


@app.route("/")
@limiter.limit("1/second", override_defaults=False)
def index():
    return "Hello"


@app.route("/ping")
@limiter.exempt
def ping():
    return "PONG"

if __name__ == "__main__":
    app.run(debug=True)
